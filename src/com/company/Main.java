package com.company;

import com.company.model.Book;

public class Main {

    public final static int COUNT = 5;
    public static final String AUTHOR = "Грязный Билли";
    private final static int YEAR = 1500;

    public static void main(String[] args) {

        Library library = new Library(Generator.generate());


        for (Book book : library.books) {
            if (book.hasCoAuthor(AUTHOR)) {
                book.print();
            }
        }

        for (Book book : library.books) {
            if (book.authors.length >= 3) {
                book.print();
            }
        }

    }
}
