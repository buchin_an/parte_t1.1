package com.company.model;

public class Book {

    public String[] authors;
    private String title;
    public int year;
    private String publishing;


    public Book(String[] authors, String title, int year, String publishing) {
        this.authors = authors;
        this.title = title;
        this.year = year;
        this.publishing = publishing;
    }

    public void print() {
        StringBuilder sb = new StringBuilder();
        for (String author : authors) {
            sb.append(author).append("/ ");
        }
        System.out.println(sb.toString());
        System.out.println("Name " + title);
        System.out.println("Year " + year);
        System.out.println("Publishing " + publishing);
        System.out.println("--------------------------");
    }
    public boolean hasCoAuthor(String name) {
        for (String author : authors) {
            if ((author.equalsIgnoreCase(name)) && (authors.length > 1)) {
                return true;
            }
        }
        return false;
    }


}
