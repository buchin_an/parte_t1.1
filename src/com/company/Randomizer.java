package com.company;

import java.util.Random;

public class Randomizer {

    public String getTitle() {
        String[] firsts = {"Ползущие", "Похотливые", "Смертоносные", "Фейсбучные", "Систематические"};
        String[] seconds = {"чихуахуа", "водолазы", "супергерои", "инопланетяне", "развратницы"};
        return new StringBuilder()
                .append("\"")
                .append(firsts[new Random().nextInt(5)])
                .append(" ")
                .append(seconds[new Random().nextInt(5)])
                .append("\"").toString();
    }

    public String getAuthor() {
        String[] firsts = {"Грязный", "Неумолимый", "Одноногий", "Кот", "Какой-то"};
        String[] seconds = {"Билли", "Ктулху", "Пират", "Бздашек", "Гендальф"};
        return new StringBuilder()
                .append(firsts[new Random().nextInt(5)])
                .append(" ")
                .append(seconds[new Random().nextInt(5)]).toString();
    }

    public int getYear() {
        return 1000 + new Random().nextInt(1000);
    }

    public String getPublishing() {
        String[] firsts = {"Пив", "Мега", "Гермо", "Нано", "Мили"};
        String[] seconds = {"няша", "булька", "синька", "вушка", "енька"};
        return new StringBuilder()
                .append(firsts[new Random().nextInt(5)])
                .append(seconds[new Random().nextInt(5)]).toString();
    }


}

