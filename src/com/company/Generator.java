package com.company;

import com.company.model.Book;

import java.util.Random;

public class Generator {
    public static Book[] generate() {


        Randomizer randomizer = new Randomizer();

        Book[] books = new Book[Main.COUNT];

        String[] authors = new String[new Random().nextInt(5)];
        for (int i = 0; i < authors.length; i++) {
            authors[i] = randomizer.getAuthor();
        }

        for (int i = 0; i < books.length; i++) {
            books[i] = new Book(authors, randomizer.getTitle(), randomizer.getYear(), randomizer.getPublishing());
        }
        return books;
    }
}
